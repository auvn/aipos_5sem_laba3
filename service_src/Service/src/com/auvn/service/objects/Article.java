package com.auvn.service.objects;

import java.io.Serializable;

public class Article implements Serializable {
	private String name = "New Article";
	private String data;
	private int id;

	public Article(String name, String data) {
		this.name = name;
		this.data = data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
