package com.auvn.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.auvn.service.objects.Article;
import com.auvn.service.objects.Category;

public class Service implements Serializable {
	private File dbFile = new File("db_data");
	private Category rootCategory;
	private ArrayList<String> tempList;

	public Service() {
		try {
			init();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String addCategory(String categoryPath, String subCategoryName)
			throws FileNotFoundException, IOException {
		Category categoryForNew = rootCategory;
		if (!categoryPath.equals("")) {
			categoryForNew = rootCategory.findCategory(
					categoryPath.split("[/]"), 0);
			for (Category subCategory : categoryForNew.getSubCategories()) {
				if (subCategory.getName().equals(subCategoryName))
					return "Fail";
			}
		}
		categoryForNew.addCategory(subCategoryName);
		flushDB();
		return "Ok";
	}

	public String removeCategory(String categoryPath, String categoryName)
			throws FileNotFoundException, IOException {
		if (!categoryPath.equals("")) {
			Category categoryForRemove = rootCategory.findCategory(
					categoryPath.split("[/]"), 0);
			for (Category subCategory : categoryForRemove.getSubCategories()) {
				if (subCategory.getName().equals(categoryName)) {
					categoryForRemove.removeCategory(subCategory);
					flushDB();
					return "Ok";
				}
			}
		}
		return "Fail";
	}

	public String editCategory(String oldCategoryPath, String newCategoryName)
			throws FileNotFoundException, IOException {
		System.out.println(oldCategoryPath);
		if (!oldCategoryPath.equals("/")) {
			Category categoryForEdit = rootCategory.findCategory(
					oldCategoryPath.split("[/]"), 0);
			if (categoryForEdit.getName().length() != 0) {
				categoryForEdit.setName(newCategoryName);
				flushDB();
				return "Ok";
			}
		}
		return "Fail";
	}

	public String[] getSubCategories(String categoryName) {
		List<Category> categories = rootCategory.findCategory(
				categoryName.split("[/]"), 0).getSubCategories();
		
		String[] categoriesObjects = new String[categories.size()];
		for (int i = 0; i < categories.size(); i++) {
			categoriesObjects[i] = categories.get(i).getName();
		}
		
		return categoriesObjects;
	}

	public String getRootCategoryName() {
		return rootCategory.getName();
	}

	public String addArticle(String articlePath, String newArticleName,
			String newArticleData) throws FileNotFoundException, IOException {
		Category category = rootCategory.findCategory(articlePath.split("[/]"),
				0);
		if (category.getName() != null) {
			if (category.findArticle(newArticleName).getName() == null) {
				category.addArticle(new Article(newArticleName, newArticleData));
				flushDB();
				return "Ok";
			}
		}
		return "Fail";
	}

	public String removeArticle(String articlePath, String articleName)
			throws FileNotFoundException, IOException {
		Category category = rootCategory.findCategory(articlePath.split("[/]"),
				0);
		Article oldArticle = category.findArticle(articleName);
		if (oldArticle.getName() != null) {
			category.removeArticle(oldArticle);
			flushDB();
			return "Ok";
		}
		return "Fail";
	}

	public String[] getArticleInfo(String articlePath, String articleName) {
		Article article = rootCategory
				.findCategory(articlePath.split("[/]"), 0).findArticle(
						articleName);

		if (article.getName() != null) {
			return new String[] { article.getName(), article.getData() };
		}
		return new String[]{};
	}

	public String editArticle(String articlePath, String oldArticleName,
			String newArticleName, String newArticleData)
			throws FileNotFoundException, IOException {
		Article oldArticle = rootCategory.findCategory(
				articlePath.split("[/]"), 0).findArticle(oldArticleName);
		if (oldArticle.getName() != null) {
			System.out.println("Name: " + newArticleName);
			oldArticle.setName(newArticleName);
			System.out.println("Data: " + newArticleData);
			oldArticle.setData(newArticleData);
			flushDB();
			return "Ok";
		}
		return "Fail";
	}

	public String[] getArticles(String categoryName) {
		tempList.clear();
		List<Article> articles = rootCategory.findCategory(
				categoryName.split("[/]"), 0).getArticles();
		String[] articlesObjects = new String[articles.size()];
		for (int i = 0; i < articles.size(); i++) {
			articlesObjects[i] = articles.get(i).getName();
		}
		return articlesObjects;
	}

	private void flushDB() throws FileNotFoundException, IOException {
		ObjectOutputStream dbFileObjectOutputStream = new ObjectOutputStream(
				new FileOutputStream(dbFile));
		dbFileObjectOutputStream.writeObject(rootCategory);
		dbFileObjectOutputStream.flush();
		dbFileObjectOutputStream.close();
	}

	public void init() throws IOException, ClassNotFoundException {
		if (!dbFile.exists()) {

			rootCategory = new Category("/");
			rootCategory.addCategory("new");
			flushDB();
			System.out.println("db file created");
		} else {
			ObjectInputStream dbFileObjectInputStream = new ObjectInputStream(
					new FileInputStream(dbFile));
			rootCategory = (Category) dbFileObjectInputStream.readObject();
			dbFileObjectInputStream.close();
		}

		tempList = new ArrayList<String>();
	}

	public Category getRootCategory() {
		return rootCategory;
	}
}
